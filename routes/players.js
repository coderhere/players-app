const express = require("express");
const router = express.Router();
const db = require("../db");

function getTimeString(localTime){
  var t = new Date(localTime);
  t = t.toISOString().split(".")[0]
  t = t.replace("T"," ").replace("Z","");
  return t
}


router.get("/", async function(req, res, next) {
  try {
    const results = await db.query("SELECT * FROM players order by score desc");
    return res.json(results.rows);
  } catch (err) {
    return next(err);
  }
});

router.post("/", async function(req, res, next) {
  console.log(req.body.name)
  try {
    var current_timestamp=getTimeString(new Date());
    const result = await db.query(
      "INSERT INTO players (name,score,created_at,email) VALUES ($1,$2,$3,$4) RETURNING *",
      [req.body.name, req.body.score,current_timestamp,req.body.email]
    );
    return res.json(result.rows[0]);
  } catch (err) {
    return next(err);
  }
});

router.patch("/:id", async function(req, res, next) {
  console.log(req.body.name)
  try {
    const result = await db.query(
      "UPDATE players SET name=$1, score=$2 WHERE id=$3 RETURNING *",
      [req.body.name, req.body.score, req.params.id]
    );
    console.log("UPDATE players SET name=$1, score=$2 WHERE id=$3 RETURNING *",
      [req.body.name, req.body.score, req.params.id])
    console.log(result.rows[0]);
    return res.json(result.rows[0]);
  } catch (err) {
    return next(err);
  }
});

router.delete("/:id", async function(req, res, next) {
  try {
    const result = await db.query("DELETE FROM players WHERE id=$1", [
      req.params.id
    ]);
    return res.json({ message: "Deleted" });
  } catch (err) {
    return next(err);
  }
});

module.exports = router;