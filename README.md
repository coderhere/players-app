players-app

This is the backend code for players arena

To run this repository follow the below steps 

Clone this code into your local system 

Ensure your have node installed and postgres installed on your system

Run this to create a db -> createdb players-app

Enter into postgres using psql players-app and create a table using -> CREATE TABLE players (id SERIAL PRIMARY KEY, name TEXT, score integer,created_at TIMESTAMP WITHOUT TIME ZONE,email character varying(50) );

Modify the settings in index.js file which is inside db folder as per your postgres config 

Enter into terminal where the repository is cloned and do npm install and then run nodemon app.js which will start a server on port 3000

