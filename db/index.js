const { Client } = require("pg");
const client = new Client({
  connectionString: "postgresql://yogesh_extra:yogesh_extra@localhost/players-app"
});

client.connect();

module.exports = client;